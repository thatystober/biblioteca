<?php namespace App\Models;
    
    use CodeIgniter\Model;

    class LocacaoModel extends Model{
        protected $DBGroup = 'default';
        protected $table = 'locacao';
        protected $allowedFields = ['id-loc', 'id-livro', 'data-loc', 'data-dev', 'id-us', 'multa'];

        public function listar(){
            return $this->findAll();
        }
    }

?>