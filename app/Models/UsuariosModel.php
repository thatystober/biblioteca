<?php namespace App\Models;

use CodeIgniter\Model;

class UsuariosModel extends Model
{
    protected $DBGroup = 'default';
    protected $table = 'clientes';
    protected $allowedFields = ['matricula', 'nome', 'email', 'senha'];

    public function listar(){
        return $this->findAll();
    }
}