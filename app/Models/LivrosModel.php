<?php namespace App\Models;

use CodeIgniter\Model;

class LivrosModel extends Model
{
    protected $DBGroup = 'default';
    protected $table = 'livros';
    protected $allowedFields = ['isbn','nome','autor','editora', 'ano'];

    public function listar(){
        return $this->findAll();
    }
}