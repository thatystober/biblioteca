<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\LivrosModel;

class Livros extends Controller{
    public function lista(){
        $model = new LivrosModel();
        $data['livros'] = $model->listar();
        $data['cabecalho'] = array('Isbn', 'Nome', 'Autor', 'Editora', 'Ano');
        
        return view('tabela-livros', $data);
    }

    public function novo(){
        return view('inserir-livros');
    }

    public function inserir(){
        $model = new LivrosModel();

        $model->save([
            'isbn'    => $this->request->getPost('isbn'),
            'nome'    => $this->request->getPost('nome'),
            'autor'   => $this->request->getPost('autor'),
            'editora' => $this->request->getPost('editora'),
            'ano'     => $this->request->getPost('ano')
        ]);

        echo "Livro adicionado com sucesso!";
        return view('inserir-livros');
    }

    public function edit($id = null){
        $model = new LivrosModel();

        $data['livro'] = $model->where('id', $id)->first();

        return view('atualizar-livros', $data);
    }

    public function atualizar(){
        $model = new LivrosModel();
        $id = $this->request->getVar('id');

        $data =[
            'isbn'    => $this->request->getVar('isbn'),
            'nome'    => $this->request->getVar('nome'),
            'autor'   => $this->request->getVar('autor'),
            'editora' => $this->request->getVar('editora'),
            'ano'     => $this->request->getVar('ano')
        ];
        
        $save = $model->update($id, $data);

        return $this->lista();
    }

    
    public function delete($id = null){
        $model = new LivrosModel();
        $data['nome'] = $model->where('id', $id)->delete();
		return $this->lista();
    }
}
?>