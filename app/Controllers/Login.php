<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\UsuariosModel;

class login extends Controller{
    public function index(){

        $data = [];
        
        helper(['form-login']);

        if ($this->request->getMethod() == 'post'){
            $rules = [
                'email' =>  'required',
                'senha' =>  'required|validateUser[email, senha]'
            ];

            $erros = [
                'senha' => [
                    'validateUser' => 'Email ou Senha Incorreto'
                ]
            ];

            if (! $this->validate($rules, $erros)){
                $data['validation'] = $this->validator;
            } else{
                $model = new UsuariosModel();

                $usuario = $model->model('email', $this->request->getVar('email'))
                        -> first();
                $this->setUserMethod($usuario);

                return redirect()->to('usuarios');
            }
        }

        echo view('form-login.php', $data);
    }

    private function setUserMethod($usuario){
        $data = [
            'id'        => $usuario['id'], 
            'matricula' => $usuario['matricula'],
            'nome'      => $usuario['nome'],
            'email'     => $usuario['email'],
            'isLoggedIn' => TRUE

        ];

        session()->set($data);

        return true;
    }
}

?>