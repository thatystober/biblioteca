<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\UsuariosModel;

class Usuarios extends Controller{
    public function lista(){
        $model = new UsuariosModel();
        $data['usuarios'] = $model->listar();
        $data['cabecalho'] = array('Matricula', 'Nome', 'Email', 'Senha');

        return view('tabela-usuarios', $data);
    }

    public function novo(){
        return view('inserir-usuarios');
    }

    public function inserir(){
        $model = new UsuariosModel();

        $model->save([
            'matricula' => $this->request->getPost('matricula'),
            'nome'      => $this->request->getPost('nome'),
            'email'     => $this->request->getPost('email'),
            'senha'     => $this->request->getPost('senha')
        ]);
            
        echo 'Usuário adicionado com sucesso!';

        return view('inserir-usuarios');
    }

    public function edit($id = null){
        $model = new UsuariosModel();

        $data['usuario'] = $model->where('id', $id)->first();

        return view('atualizar-usuarios', $data);
    }

    public function atualizar(){
        $model = new UsuariosModel();
        $id = $this->request->getVar('id');

        $data =[
            'nome'        => $this->request->getVar('nome'),
            'email'    => $this->request->getVar('email'),
        ];
        
        $save = $model->update($id, $data);

        return $this->lista();
    }

    public function delete($id = null){
        $model = new UsuariosModel();
        $data['nome'] = $model->where('id', $id)->delete();
        
		return $this->lista();
    }
}