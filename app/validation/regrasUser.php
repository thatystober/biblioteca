<?php
namespace App\Validation;

use App\Models\UsuariosModel;

class RegrasUser{
    public function validateUser(string $str, string $fields, array $data){
        $model = new UsuariosModel;
        $usuario = $model->where('email', $data['email'])
                    ->first();

        if(!$usuario){
            return false;
        }else{
            return password_verify($data['senha'], $usuario['senha']);
        }
    }
}

?>