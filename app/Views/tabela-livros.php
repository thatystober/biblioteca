<h1>Estante de Livros</h1>

<?php echo anchor('livros/novo','Novo Livro');?> <br />

<table>
    <tr>
        <th><?php echo($cabecalho[0])?></th>
        <th><?php echo($cabecalho[1])?></th>
        <th><?php echo($cabecalho[2])?></th>
        <th><?php echo($cabecalho[3])?></th>
        <th><?php echo($cabecalho[4])?></th>
    </tr>

    <?php foreach ($livros as $cont): ?>
        <tr>
            <td><?php echo $cont["isbn"]; ?></td>
            <td><?php echo $cont["nome"]; ?></td>
            <td><?php echo $cont["autor"]; ?></td>
            <td><?php echo $cont["editora"]; ?></td>
            <td><?php echo $cont["ano"]; ?></td>

            <td><a href="<?php echo base_url();?>/livros/edit/ <?php echo $cont['id']; ?>">Edit</a>&nbsp;
            <a href="<?php echo base_url(); ?>/livros/delete/<?php echo $cont['id']; ?>">Delete</a></td>
        </tr>
    <?php endforeach; ?> 
</table>
</body>
</html> 

</table>