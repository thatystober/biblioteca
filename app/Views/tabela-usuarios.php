<h1>Usuários</h1>

<?php echo anchor('usuarios/novo','Novo Usuário');?> <br />

<table>
    <tr>
        <th><?php echo($cabecalho[0])?></th>
        <th><?php echo($cabecalho[1])?></th>
        <th><?php echo($cabecalho[2])?></th>
    </tr>

    <?php foreach($usuarios as $cont):?>
        <tr>
            <th><?php echo $cont['matricula']?></th>
            <th><?php echo $cont['nome']?></th>
            <th><?php echo $cont['email']?></th>

            <td><a href="<?php echo base_url();?>/usuarios/edit/ <?php echo $cont['id']; ?>">Edit</a>&nbsp;
            <a href="<?php echo base_url(); ?>/usuarios/delete/<?php echo $cont['id']; ?>">Delete</a></td>
        </tr>
    <?php endforeach ?>
</table>
