<h1>Usuários - Cadastro</h1>

<?= \Config\Services::validation()->listErrors(); ?>

<form action="/usuarios/inserir" method="post">
<?= csrf_field() ?>

    <label for="matricula">Digite sua Matricula: </label> 
    <input type="number" name="matricula"><br />

    <label for="nome">Digite seu nome: </label>
    <input type="text" name="nome"><br />

    <label for="email">Digite seu Email:</label>
    <input type="text" name="email"><br />

    <label for="senha">Digite uma senha:</label>
    <input type="number" name="senha"><br />

    <input type="submit" name="submit" value="Salvar" />
</form>