<h1>Insira Livros</h1>

<?= \Config\Services::validation()->listErrors(); ?>

<form action="/livros/inserir" method="post">
<?= csrf_field() ?>

    <label for="isbn">ISBN</label>
    <input type="input" name="isbn" /><br />

    <label for="nome">Nome do Livro</label>
    <input type="input" name="nome" /><br />

    <label for="text">Autor</label>
    <input type="input" name="autor"><br />

    <label for="text">Editora</label>
    <input type="input" name="editora"><br />

    <label for="number">Ano</label>
    <input type="input" name="ano"><br />

    <input type="submit" name="submit" value="Salvar" />
</form>