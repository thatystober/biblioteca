<h1>Atualizar Usuário</h1>

<?= \Config\Services::validation()->listErrors(); ?>

<form action="/usuarios/atualizar" method="post" name="form-atualizar">
<?= csrf_field() ?>
    <input type="hidden" name="id" value="<?php echo $usuario["id"] ?>"><br /> 

    <label for="matricula">Matricula: <?php echo $usuario['matricula']?> </label> <br />

    <label for="nome">Digite seu nome: </label>
    <input type="text" name="nome" value="<?php echo $usuario['nome']?>"><br />

    <label for="email">Digite seu Email:</label>
    <input type="text" name="email" value="<?php echo $usuario['email']?>"><br />

    <input type="hidden" name="senha" value="<?php echo $usuario["senha"] ?>"><br /> 

    <input type="submit" value="Edit" />
</form>