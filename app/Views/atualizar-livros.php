<h1>Atualizar Estante de Livros</h1>

<?= \Config\Services::validation()->listErrors(); ?>

<form action="/livros/atualizar" method="post" name="form-atualizar">
<?= csrf_field() ?>

    <input type="hidden" name="id" value="<?php echo $livro["id"] ?>"><br /> 

    <label for="isbn">ISBN:</label>
    <input type="text" name="isbn" value="<?php echo $livro["isbn"] ?>"><br />  

    <label for="nome">Nome:</label>
    <input type="text" name="nome" value="<?php echo $livro["nome"] ?>"><br />

    <label for="editora">Editora:</label>
    <input type="text" name="editora" value="<?php echo $livro["editora"] ?>"><br />

    <label for="ano">Autor:</label>
    <input type="text" name="autor" value="<?php echo $livro["autor"] ?>"><br />

    <label for="ano">Ano de Publicação:</label>
    <input type="number" name="ano" value="<?php echo $livro["ano"] ?>"><br />


    <input type="submit" value="Edit">
</form>
